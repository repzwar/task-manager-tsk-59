package ru.pisarev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pisarev.tm.api.repository.model.IProjectRepository;
import ru.pisarev.tm.api.service.model.IProjectService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.ProjectGraph;
import ru.pisarev.tm.model.UserGraph;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<ProjectGraph> collection) {
        if (collection == null) return;
        for (ProjectGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public ProjectGraph add(@Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        repository.add(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<ProjectGraph> projects = repository.findAll();
        for (ProjectGraph t :
                projects) {
            repository.remove(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final ProjectGraph project = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
        if (project == null) return;
        repository.remove(project);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @Nullable final ProjectGraph project = repository.getReference(entity.getId());
        if (project == null) return;
        repository.remove(project);
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
        @Nullable final ProjectGraph project = repository.findByIndex(userId, index);
        if (project == null) return;
        repository.remove(project);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectGraph project = repository.findByName(userId, name);
        if (project == null) return;
        repository.remove(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        return project;
    }

    @Nullable
    public ProjectGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = new ProjectGraph(name, description);
        project.setUser(user);
        return add(project);
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull final String userId) {

        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<ProjectGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (ProjectGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph add(@NotNull final UserGraph user, @Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final ProjectGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final List<ProjectGraph> projects = repository.findAllByUserId(userId);
        for (ProjectGraph t :
                projects) {
            repository.remove(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final ProjectGraph project = repository.findByIdUserId(
                userId,
                optionalId.orElseThrow(EmptyIdException::new)
        );
        if (project == null) return;
        repository.remove(project);
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @Nullable final ProjectGraph project = repository.findByIdUserId(userId, entity.getId());
        if (project == null) return;
        repository.remove(project);
    }

}
