package ru.pisarev.tm.repository;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

public class AbstractRepository {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    public void close() {
        entityManager.close();
    }

    public EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }
}
