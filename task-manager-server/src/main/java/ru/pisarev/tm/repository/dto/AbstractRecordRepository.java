package ru.pisarev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.AbstractRecord;
import ru.pisarev.tm.repository.AbstractRepository;

import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRecordRepository<E extends AbstractRecord> extends AbstractRepository {

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            add(item);
        }
    }

    protected E getSingleResult(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        if (list.isEmpty()) return null;
        return list.get(0);
    }

}